#include "benchmark/benchmark.h"
#include <string_view>
#include <array>
#include <ranges>
#include <limits>
#include <cstdint>
#include <numeric>
#include <algorithm>
#include <iostream>

namespace
{
    using integer_type = std::uint64_t;

    static constexpr auto alphabet = std::string_view("abcdefghijklmnopqrstuvwxyz");
    static constexpr auto assumed_alphabet_length = std::size_t(alphabet.size());
    static constexpr auto max_word_length = integer_type(24);
    static constexpr auto test_word_count = integer_type(1000);
    static constexpr auto factorial_lut_size = integer_type(10);

    using letter_count_lut = std::array<integer_type, assumed_alphabet_length>;
    using unique_letter_lut = std::array<integer_type const *, assumed_alphabet_length>;

    static constexpr auto factorial(integer_type v) -> integer_type
    {
        if (v == 0 || v == 1) return 1;
        return v * factorial(v - 1);
    }

    static constexpr auto small_factorial_lut = []
    {
        std::array<integer_type, factorial_lut_size> output;
        for (integer_type i = 0; i < factorial_lut_size; ++i)
        {
            output[i] = factorial(i);
        }
        return output;
    }();

    static constexpr auto one = integer_type(1);

    static char random_letter()
    {
        return alphabet[rand() % assumed_alphabet_length];
    }

    static std::string random_word(int max_len = 24)
    {
        std::string output;
        int max = rand() % max_len + 1;
        for (int i = 0; i < max; ++i)
        {
            output += random_letter();
        }
        return output;
    }

    static std::vector<std::string> random_word_list(int max_len, int count)
    {
        std::vector<std::string> output;
        for (int i = 0; i < count; ++i)
        {
            output.push_back(random_word(max_len));
        }
        return output;
    }

    static auto random_words = random_word_list(max_word_length, test_word_count);

    struct UniqueCombinations
    {
        auto begin() noexcept { return dense.begin(); }
        auto end() noexcept { return begin() + dense_length; }
        auto begin() const noexcept { return dense.begin(); }
        auto end() const noexcept { return begin() + dense_length; }

        UniqueCombinations(std::string_view input)
            : input_length(input.size())
        {
            for (auto const & v : input)
            {
                auto & letter = letters[static_cast<uint8_t>(v) - alphabet.front()];
                if (++letter == 2) { dense[dense_length++] = &letter; }
            }
            std::sort(begin(), end(), [](integer_type const * lhs, integer_type const * rhs)
            {
                return *lhs < *rhs;
            });
        }

        auto Eval(auto factorial_fn, auto & cache) const noexcept -> integer_type
        {
            auto const den = std::accumulate(begin(), end(), one, [&](integer_type total, integer_type const * count) -> integer_type
            {
                return total * factorial_fn(*count, cache);
            });
            auto const num = factorial_fn(input_length, cache);
            return num / den;
        }

        letter_count_lut letters{};
        unique_letter_lut dense{};
        std::size_t dense_length = 0;
        std::size_t input_length = 0;
    };
}

namespace seppeon
{
    struct factorial_cache
    {
        integer_type last_v = factorial_lut_size + 1;
        integer_type last_result = factorial(factorial_lut_size + 1);
    };

    auto ordered_factorial(integer_type v, factorial_cache & cache) -> integer_type
    {
        if (v < small_factorial_lut.size()) {
            return small_factorial_lut[v];
        }
        if (cache.last_v == v) {
            return cache.last_result;
        }
        auto result = cache.last_result;
        for (auto i = cache.last_v; i < v+1; ++i) {
            result *= i;
        }
        cache.last_result = result;
        cache.last_v = v;
        return result;
    }

    auto arrangements(std::string_view input, factorial_cache & cache) -> integer_type
    {
        return UniqueCombinations(input).Eval(ordered_factorial, cache);
    }
}

namespace chris
{
    struct factorial_cache
    {
        std::vector<integer_type> memo{ small_factorial_lut.begin(), small_factorial_lut.end() };
    };

    auto memoized_factorial(integer_type const n, factorial_cache & cache) -> integer_type
    {
        auto max_factorial = std::max<std::size_t>(1, cache.memo.size()) - 1;
        if (n < cache.memo.size()) {
            return cache.memo[n];
        }
        for (auto i = max_factorial; i < n + 1; ++i) {
            cache.memo.push_back(i * cache.memo.back());
        }
        return cache.memo[n];
    }

    auto arrangements(std::string_view input, factorial_cache & cache) -> integer_type
    {
        return UniqueCombinations(input).Eval(memoized_factorial, cache);
    }
}

namespace
{
    void SeppeonArrangements(benchmark::State& state) {
        for (auto _ : state) {
            for (std::string_view test : random_words)
            {
                seppeon::factorial_cache cache;
                auto result = seppeon::arrangements(test, cache);
                benchmark::DoNotOptimize(result);
            }
        }
    }

    void ChrisArrangements(benchmark::State& state) {
        for (auto _ : state) {
            chris::factorial_cache cache;
            for (std::string_view test : random_words)
            {
                auto result = chris::arrangements(test, cache);
                benchmark::DoNotOptimize(result);
            }
        }
    }
}

BENCHMARK(SeppeonArrangements);
BENCHMARK(ChrisArrangements);