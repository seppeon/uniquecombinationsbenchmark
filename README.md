max_word_length: 30
test_word_count: 100
factorial_lut_size: 10
```
Run on (32 X 3400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x16)
  L1 Instruction 32 KiB (x16)
  L2 Unified 512 KiB (x16)
  L3 Unified 32768 KiB (x2)
```
|Benchmark             |       Time     |       CPU |  Iterations|
|----------------------|----------------|-----------|------------|
|SeppeonArrangements   |    3369 ns     |   3348 ns |      224000|
|ChrisArrangements     |    3353 ns     |   3369 ns |      213333|

---

max_word_length: 24
test_word_count: 100
factorial_lut_size: 10
```
Run on (32 X 3400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x16)
  L1 Instruction 32 KiB (x16)
  L2 Unified 512 KiB (x16)
  L3 Unified 32768 KiB (x2)
```
|Benchmark             |      Time     |      CPU  |Iterations|
|----------------------|---------------|-----------|----------|
|SeppeonArrangements   |   2805 ns     |  2717 ns  |    235789|
|ChrisArrangements     |   2888 ns     |  2825 ns  |    248889|

---
max_word_length: 24
test_word_count: 1000
factorial_lut_size: 10
```
Run on (32 X 3400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x16)
  L1 Instruction 32 KiB (x16)
  L2 Unified 512 KiB (x16)
  L3 Unified 32768 KiB (x2)
```
|Benchmark              |      Time |            CPU |  Iterations|
|-----------------------|-----------|----------------|------------|
|SeppeonArrangements    |  27098 ns |       27274 ns |       26353|
|ChrisArrangements      |  27471 ns |       26995 ns |       24889|
